﻿using UnityEngine;
using System.Collections;

public class LevelScript : MonoBehaviour {
    public int moneyToNextLvl;
    public GameObject loadScreen;
    Statistics personStats;

	// Use this for initialization
	void Start () {
        Cursor.visible = false;
        personStats = GameObject.Find("Player").GetComponent<Statistics>();
	}

    public void CheckMoney()
    {
        if (personStats.GetMoney() >= moneyToNextLvl)
            StartCoroutine(DisplayLoadScreen(2));
            //Application.LoadLevel(2);
    }

    IEnumerator DisplayLoadScreen(int levelIndex)
    {
        loadScreen.SetActive(true);
        AsyncOperation async = Application.LoadLevelAsync(levelIndex);
        while (!async.isDone)
        {
            Debug.Log(async.progress);
            yield return null;
        }
       
    }
}
