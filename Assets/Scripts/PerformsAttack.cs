﻿using UnityEngine;
using System.Collections;

public class PerformsAttack : MonoBehaviour {

	public float range = 100.0f;

	public float cooldown = 0.2f;
	float cooldownRemaining = 0;

	public GameObject debrisPrefab;
	public GameObject smokePrefab;
	public GameObject laserPrefab;
	LineRenderer lineRenderer;

	public float damage = 50f;

	GameObject gunBarrel;

	// Use this for initialization
	void Start () {
		lineRenderer = Instantiate (laserPrefab).GetComponent<LineRenderer>();
		lineRenderer.GetComponent<Renderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		cooldownRemaining -= Time.deltaTime;

		if (Input.GetMouseButton(0) && cooldownRemaining <= 0) {
			cooldownRemaining = cooldown;

			Ray ray = new Ray (Camera.main.transform.position, Camera.main.transform.forward);
			RaycastHit hitInfo;

			if (Physics.Raycast(ray, out hitInfo, range)) {
				Vector3 hitPoint = hitInfo.point;
				GameObject go = hitInfo.collider.gameObject;
				Debug.Log ("Hit Object: " + go.name);   
				Debug.Log ("Hit Object Tag: " + go.tag);
				Debug.Log ("Hit Point: " + hitPoint);

				HasHealth h = go.GetComponent<HasHealth>();

				if (h != null) {
					h.ReceiveDamage (damage);
				}

				if (go.tag != "Enemy") {
					if (debrisPrefab != null) {
						Instantiate (debrisPrefab, hitPoint + hitInfo.normal * 0.01f, Quaternion.FromToRotation(Vector3.forward, hitInfo.normal));
					}
				}

				if (smokePrefab != null) {
					Instantiate (smokePrefab, hitPoint, Quaternion.identity);
				}

				gunBarrel = GameObject.FindWithTag("GunBarrel");
				Vector3 laserPosition = gunBarrel.transform.position - gunBarrel.transform.forward*-1f;

				if (lineRenderer != null) {
					lineRenderer.GetComponent<Renderer>().enabled = true;
					lineRenderer.SetPosition(0, laserPosition);
					lineRenderer.SetPosition(1, hitPoint);
					StartCoroutine(HideLaser());
				}
			}
		}
	}

	IEnumerator HideLaser () {
		yield return new WaitForFixedUpdate();
		lineRenderer.GetComponent<Renderer>().enabled = false;
	}
}
