﻿using UnityEngine;
using System.Collections;

public class FPC : MonoBehaviour
{
    public float speedMove;
    public float speedRun;
    public float speedRotarion;
    public float maxUpDownRotation;
    public float jump;
    public float speedDrop;
    public float deadSpeedDrop;


    float zVector;
    float xVector;
    float velocity = 0;

    float rotLeftRight;
    float rotUpDown = 0;

    bool run;

    Vector3 move;
    Vector3 rotation;
    CharacterController character;
    Gun gun;
    ChooseGun chooseGun;
    Statistics statistics;
    GameObject currentGun;

    // Use this for initialization
    void Start()
    {
        run = false;
        Cursor.visible = false;
        character = GetComponent<CharacterController>();
        statistics = GameObject.Find("Player").GetComponent<Statistics>();

        if (Application.loadedLevel != 1)
        {
            chooseGun = GameObject.Find("Gun").GetComponent<ChooseGun>();
            currentGun = chooseGun.GetCurrentGun();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //1 = fisrt gameplay level - don't have shot
        if (Application.loadedLevel != 1)
        {
            currentGun = chooseGun.GetCurrentGun();
            gun = currentGun.GetComponent<Gun>();
            if (!gun)
                Debug.Log("ZLY GUN!");
        }

        if (!gun)
            Debug.Log("ZLY GUN2!");

        //Rotation
        rotLeftRight = Input.GetAxis("Mouse X") * speedRotarion;
        transform.Rotate(0, rotLeftRight, 0);

        rotUpDown -= Input.GetAxis("Mouse Y") * speedRotarion;
        rotUpDown = Mathf.Clamp(rotUpDown, -maxUpDownRotation, maxUpDownRotation);

        Camera.main.transform.localRotation = Quaternion.Euler(rotUpDown, 0, 0);
        if (currentGun && gun)
            currentGun.transform.localRotation = Camera.main.transform.localRotation;

        //Movement
        zVector = Input.GetAxis("Vertical");
        xVector = Input.GetAxis("Horizontal");

        if (character.isGrounded)
        {
            if (Input.GetButtonDown("Jump"))
                velocity = jump;
            else
                velocity = Physics.gravity.y * speedDrop;

            if (Input.GetButton("Run") && zVector > 0)
                run = true;
            else if (!Input.GetButton("Run"))
                run = false;

            if (velocity <= -deadSpeedDrop)
                statistics.Dead();
        }
        else
            velocity += Physics.gravity.y * Time.deltaTime * speedDrop;

        if (run)
            move = new Vector3(xVector * speedRun, velocity, zVector * speedRun);
        else
            move = new Vector3(xVector * speedMove, velocity, zVector * speedMove);

        move = transform.rotation * move;
        character.Move(move * Time.deltaTime);

        //Shot
        if (Input.GetButton("Fire1") && gun)
            gun.Shot(); 
        else if (gun)
            gun.LoadingCooldown();
    }
}
