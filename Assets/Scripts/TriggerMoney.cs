﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TriggerMoney : MonoBehaviour { 
    Statistics personStats;
    LevelScript lvl;
    UI ui;
    int moneys = 1;
    // Use this for initialization
    void Start()
    {
        ui = GameObject.Find("level_01").GetComponent<UI>();
        personStats = GameObject.Find("Player").GetComponent<Statistics>();
        lvl = GameObject.Find("level_01").GetComponent<LevelScript>();
    }
    void OnTriggerEnter(Collider other)
    {
        personStats.AddMoney(moneys);
        ui.SetUI_Lvl1();
        lvl.CheckMoney();
        Object.Destroy(this.gameObject);
    }
}
