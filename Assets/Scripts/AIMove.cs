﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class AIMove : MonoBehaviour
{
    public Transform targets;
    public float live = 100;
    NavMeshAgent agent;
    
    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.destination = targets.position;
    }
    
    // Update is called once per frame
    void Update()
    {
        agent.destination = targets.position;
    }


    public void Heart(int damage) {
        live -= damage;
        Debug.Log("Live: " + live);
        if (live <= 0)
            Destroy(gameObject);
    }
}
