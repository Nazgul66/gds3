﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Gun : MonoBehaviour
{
    //public GameObject laser;
    public GameObject laserPrefab;

    public float speedLaser;
    public float longShotInSecond;
    public float longLoadingInSecond;
    public float holdTimeInSecond;
    public int stronFire;

    float currentHoldTime;
    float cooldownPerSec;
    float loadingPerSec;
    float maxWidthCooldownImage;
    float actualCooldown;

    public RawImage cooldownImage;
    public RawImage cooldownText;
    
    void Start()
    {
        maxWidthCooldownImage = GameObject.Find("level_2").GetComponent<Lvl2Script>().GetMaxWidthCanvas();
        currentHoldTime = 0;
        actualCooldown = 0;
        cooldownImage.rectTransform.sizeDelta = new Vector2(actualCooldown, cooldownImage.rectTransform.sizeDelta.y); ;
        cooldownPerSec = maxWidthCooldownImage / longShotInSecond;
        loadingPerSec = maxWidthCooldownImage / longLoadingInSecond;
        cooldownText.enabled = false;
        UpdateCooldownImage();
    }

    public void Shot()
    {
        if (actualCooldown < maxWidthCooldownImage)
        {
            GameObject laser = (GameObject)Instantiate(laserPrefab, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
            Rigidbody rb = laser.GetComponent<Rigidbody>();
            laser.GetComponent<DestroyBolt>().damage = stronFire;
            rb.AddForce(Camera.main.transform.forward * speedLaser, ForceMode.Impulse);
            actualCooldown += cooldownPerSec * Time.deltaTime;
        }
        else
        {
            cooldownText.enabled = true;
            currentHoldTime = holdTimeInSecond;
        }
        UpdateCooldownImage();
    }

    public void LoadingCooldown()
    {
        if (currentHoldTime > 0)
            currentHoldTime -= Time.deltaTime;
        else if (actualCooldown > 0)
        {
            cooldownText.enabled = false;
            actualCooldown -= loadingPerSec * Time.deltaTime;
        }
        else if (actualCooldown < 0)
            actualCooldown = 0;
        UpdateCooldownImage();
    }

    void UpdateCooldownImage()
    {
     //   Debug.Log("update cooldown: " + actualCooldown);
        cooldownImage.rectTransform.sizeDelta = new Vector2(actualCooldown, cooldownImage.rectTransform.sizeDelta.y);
    }
}
