﻿using UnityEngine;
using System.Collections;

public class DetonatesOnHit : MonoBehaviour {

	public GameObject explosionPrefab;

	public float damage = 200f;
	public float explosionRadius = 3f;

	public Vector3 detonationPoint;

	void OnCollisionEnter() {
		Debug.Log ("OnCollisionEnter");
		Detonate();
	}

	void OnTriggerEnter() {
		Debug.Log ("OnTriggerEnter");
		Detonate();
	}

	void Detonate() {
		Vector3 explosionPoint = transform.position+detonationPoint;

		if (explosionPrefab != null) {
			Instantiate(explosionPrefab, explosionPoint, Quaternion.identity);
		}
		Destroy(gameObject);

		Collider[] colliders = Physics.OverlapSphere (explosionPoint, explosionRadius);

		foreach (Collider c in colliders) {
			HasHealth h = c.GetComponent<HasHealth>();
			if (h != null) {
				float dist = Vector3.Distance(explosionPoint, c.transform.position);
				float damageRatio = 1f - (dist / explosionRadius);

				h.ReceiveDamage (damage * damageRatio);
			}
		}
	}
}
