﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class Lvl2Script : MonoBehaviour {
    public Text scoreText;
    List<int> Best10Score;

    int score;
    float maxWidthCanvas;


    // Use this for initialization
    void Start() {
        Best10Score = new List<int>();

        LoadScore();

        RawImage rawImage = GameObject.Find("coold_down_text").GetComponent<RawImage>();

        if (!rawImage)
            Debug.Log("BAD CANVAS");

        maxWidthCanvas = rawImage.rectTransform.sizeDelta.x;

        score = 0;
        scoreText.text = "Score: " + score;
    }

    public void AddScore(int iscore) {
        score += iscore;
        UpdateScoreText();
    }

    void UpdateScoreText() {
        scoreText.text = "Score: " + score;
    }

    public float GetMaxWidthCanvas() {
        return maxWidthCanvas;
    }

    public void SaveScore() {
        Best10Score.Add(score);
        Best10Score.Sort();
        Best10Score.Reverse();

        string[] bestToSave = new string[10];
        
        for (int i = 0; i < 10; i++) {
            if (i < Best10Score.Count)
                bestToSave[i] = Best10Score[i].ToString();
        }

        ShowResult();

        File.WriteAllLines("best score.txt", bestToSave);
    }

    public void LoadScore() {
        if (File.Exists("best score.txt")) {

            string[] scores = File.ReadAllLines("best score.txt");

            foreach (var s in scores) {
                Best10Score.Add(int.Parse(s));
            }
        }
    }

    void ShowResult() {
        var UI = GameObject.Find("ResultText");
        Debug.Log(UI.name);

        foreach (var i in Best10Score)
            UI.GetComponent<Text>().text += i.ToString() + "/n";

        UI.SetActive(true);

        while (!Input.anyKeyDown) ;
    }

}
