﻿using UnityEngine;
using System.Collections;

public class PerformsAttackProjectile : MonoBehaviour {

	public float cooldown = 1f;
	float cooldownRemaining = 0;

	public GameObject projectilePrefab;
	GameObject gunBarrel;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		cooldownRemaining -= Time.deltaTime;

		if (Input.GetMouseButton(1) && cooldownRemaining <= 0) {
			cooldownRemaining = cooldown;

			gunBarrel = GameObject.FindWithTag("GunBarrel");
			Vector3 rocketPosition = gunBarrel.transform.position - gunBarrel.transform.forward*0.5f;

			Instantiate(projectilePrefab, rocketPosition, Camera.main.transform.rotation);
		}
	}
}
