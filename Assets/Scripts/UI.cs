﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UI : MonoBehaviour {
    Statistics money;
    LevelScript lvl;
    Text scoreText;
	// Use this for initialization
	void Start ()
    {
        scoreText = GameObject.Find("Score").GetComponent<Text>();
        money = GameObject.Find("Player").GetComponent<Statistics>();
        lvl = GameObject.Find("level_01").GetComponent<LevelScript>();
        SetUI_Lvl1();
	}

    public void SetUI_Lvl1()
    {
        //Tu zmień napis "Score: "
        scoreText.text = ". " + money.GetMoney().ToString() + " / " + lvl.moneyToNextLvl.ToString();
    }
}
