﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MyMenu : MonoBehaviour {
    public Button startText;
  //  public Button controlsText;
    public Button exitText;
    public GameObject loadScreen;

    // Use this for initialization
    void Start () {
        startText = startText.GetComponent<Button>();
    //    controlsText = controlsText.GetComponent<Button>();
        exitText = exitText.GetComponent<Button>();
    }
	    
    public void StartLevel()
    {
        StartCoroutine(DisplayLoadScreen(1));
    }

    /*public void ControlsPress()
    {
        Application.LoadLevel();
    }*/

    public void ExitGame()
    {
        Application.Quit();
    }

    IEnumerator DisplayLoadScreen(int levelIndex)
    {
        loadScreen.SetActive(true);
        AsyncOperation async = Application.LoadLevelAsync(levelIndex);
        while (!async.isDone)
        {
            Debug.Log(async.progress);
            yield return null;
        }

    }
}
