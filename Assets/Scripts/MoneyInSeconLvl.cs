﻿using UnityEngine;
using System.Collections;

public class MoneyInSeconLvl : MonoBehaviour {
    Lvl2Script lvl2;
    public int score;
    void Start()
    {
        lvl2 = GameObject.Find("level_2").GetComponent<Lvl2Script>();
    }
    void OnTriggerEnter(Collider other)
    {
        lvl2.AddScore(score);
        Object.DestroyObject(this.gameObject);
    }
}
