﻿using UnityEngine;
using System.Collections;

public class DestroyBolt : MonoBehaviour {
    public int damage;

    void OnTriggerEnter(Collider gameobjecy) {
        if (gameobjecy.name != "Player" && gameobjecy.tag != "Fire") {
            if (gameobjecy.tag == "Enemy") {
                Debug.Log("Enemy");
                var AIMoveObj = gameobjecy.transform.parent.GetComponentInChildren<AIMove>();
                

                if (AIMoveObj != null) {
                    Debug.Log("Heart: " + damage);
                    AIMoveObj.Heart(damage);
                } else {
                    Debug.Log("NO Heart");
                }
            } else {
                Debug.Log(gameobjecy.name);
            }
            Destroy(gameObject);
        }
    }
}


