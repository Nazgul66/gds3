﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnemy : MonoBehaviour {
    public float waitToSpawn;
    public float timeToSpawn;
    private GameObject enemyPrefab;

    // Use this for initialization
    void Start() {
        timeToSpawn = 0;
        
        enemyPrefab = Resources.Load("Prefabs/Enemy_System") as GameObject;

        if (enemyPrefab == null)
            Debug.Log("Can't load enemy prefab.");

    }

    // Update is called once per frame
    void Update() {
        if (timeToSpawn >= waitToSpawn && enemyPrefab != null) {
            var enemy = (GameObject)Instantiate(enemyPrefab, gameObject.transform.localPosition, gameObject.transform.localRotation);
            
            enemy.GetComponent<AIMove>().targets = GameObject.Find("Player").transform;
            timeToSpawn = 0;
        }else {
            timeToSpawn += Time.deltaTime;
        }
    }
}
