﻿using UnityEngine;
using System.Collections;

public class ChooseGun : MonoBehaviour
{ 
    public GameObject[] guns;
    GameObject currentGun;
    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < 4; ++i)
            if (guns[i])
                guns[i].SetActive(false);

        currentGun = guns[0];
        currentGun.SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            DeactivateGuns(0);
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            DeactivateGuns(1);
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            DeactivateGuns(2);
        else if (Input.GetKeyDown(KeyCode.Alpha4))
            DeactivateGuns(3);
    }

    void DeactivateGuns(int i)
    {
        currentGun.SetActive(false);
        currentGun = guns[i];
        if (!currentGun)
            Debug.Log("DUPA ZUPA!");
        currentGun.SetActive(true);
    }

    public GameObject GetCurrentGun()
    {
        return currentGun;
    }
}
